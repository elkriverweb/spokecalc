#include "help.h"
#include <iostream>

using namespace std;

void help() {

    // Print help message to screen
    cout << "usage: spokecalc [--version] [--help]\n" << endl;

    // About required input fields
    cout << " Required rim and hub measurements to calculate correct spoke lengths.\n" << endl;

    cout << "    Effective Rim Diameter:\n";
    cout << "\tMeasurement of rim diameter from where the heads of the spoke nipple rest in\n";
    cout << "\tthe spoke holes of the rim.\n" << endl;

    cout << "    Hub Flange Diameter:\n";
    cout << "\tMeasurement of the diameter of the circle of spoke holes in the hub flange measured\n";
    cout << "\tfrom the center of the spoke holes\n" << endl;

    cout << "    Hub Center to Flange:\n";
    cout << "\tMeasurement if the distance from the center of the hub outward to the center of the hub\n";
    cout << "\tflange. On a typical rear hub this distance will be shorter on the drive (right) side.\n" << endl;

    cout << "    Number of Spokes:\n";
    cout << "\tThe number of spokes in the wheel. Obviously, the hub and rim must have the same number\n";
    cout << "\tof holes.\n" << endl;;

    cout << "    Number of Crosses:\n";
    cout << "\tThe number of other spokes on the same flange a spoke will make when connected to the rim.\n";
    cout << "\tCommon lacing patterns are 2 or 3 cross. For example, a 3 cross lacing pattern means that\n";
    cout << "\teach trailing spoke will cross paths with 3 leading spokes on the opposite side of the\n";
    cout << "\tflange.\n" << endl;

    // About optional input fields
    cout << " Optional measurements.\n" << endl;

    cout << "   Rim Spoke Bed Offset:\n";
    cout << "\tSome rims are asymetrical and have spoke nipple holes offset to one or both sides of the\n";
    cout << "\trim. This distance is measured from the centerline of the rim outward to the center of the\n";
    cout << "\tspoke holes. This input must be a positive value if it results in the spoke nipple moving\n";
    cout << "\tfurther away from the hub flange, and a negative value if it brings the spoke nipple closer.\n" << endl;

}
